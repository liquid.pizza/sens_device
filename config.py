WIFI_SSID = "WIFI_SSID"
WIFI_PASS = "WIFI_PASS"

SERVER_URL = "MQTT_SERVER"

BASE_KEY = "sens_data"
TEMP_KEY = "temp"
HUM_KEY = "hum"
PRESS_KEY = "press"
ROOM_KEY = "kitchen"

SLEEP_TIME_S = 30
RECONNECT_TIME_S = 10