from machine import Pin, unique_id, I2C, deepsleep
import time
import network
import ubinascii
from umqtt.simple import MQTTClient

import bmp280

import config
CLIENT_ID = ubinascii.hexlify(unique_id())
SCL_PIN = 22
SDA_PIN = 21
LED_PIN = 5

MAX_NUM_CONNECTION_RETRIES = 5

# setup status LED
# the LED should be on as long, as the script is running
led = Pin(LED_PIN, Pin.OUT)

# setup BMP280 sensor
# setup I2C bus
i2c = I2C(
    scl=Pin(SCL_PIN),
    sda=Pin(SDA_PIN),
)
# create the BMP280 object
bmp = bmp280.BMP280(
    i2c_bus=i2c,
)
# setup
# TODO: why is this needed to work?
bmp.use_case(bmp280.BMP280_CASE_WEATHER)
bmp.normal_measure()

# init temp, press
temp = 0
press = 0

# setup wifi
wlan = network.WLAN(network.STA_IF) # create station interface
wlan.active(True)       # activate the interface
wlan.connect(config.WIFI_SSID, config.WIFI_PASS) # connect to an AP

print("Connecting to {}".format(config.WIFI_SSID))

# connect to the wifi
while not wlan.isconnected():
    pass

print("Connected with {}".format(config.WIFI_SSID))

print("Connecting with MQTT broker @ {}".format(config.SERVER_URL))

# create MQTT client
c = MQTTClient(CLIENT_ID, config.SERVER_URL)

def connect_to_server():
    retry_counter = 0

    # wait for a connection
    while 1:
        retry_counter += 1
        try:
            if not c.connect():
                break
        except OSError:
            print("Server not reachable.")
            print("Waiting {}s before trying to re connecting to {}".format(config.RECONNECT_TIME_S, config.SERVER_URL))
            time.sleep(config.RECONNECT_TIME_S)
            return False

        if retry_counter > MAX_NUM_CONNECTION_RETRIES:
            print("Max number of connection retries exceeded.")
            return False

    print("Connected with MQTT broker @ {}".format(config.SERVER_URL))
    return True

def generateTopic(sensor_key):
    # topic is /<BASE_KEY>/<ROOM_KEY>/<SENSOR_KEY>
    return "/{}/{}/{}".format(config.BASE_KEY, config.ROOM_KEY, sensor_key)

# main loop
if (connect_to_server()):
    # TODO: maybe move it to main.py
    try:
        # measure and get values
        temp = bmp.temperature
        press = bmp.pressure
        print("{}°C at {}hPa pressure.".format(temp, press))
    except OSError:
        print("No connection to the sensor.")

    try:
        # send data via MQTT
        temp_topic = generateTopic(config.TEMP_KEY)
        press_topic = generateTopic(config.PRESS_KEY)
        c.publish(temp_topic, str(temp), qos=1)
        print("Data publisehd {} to {}".format(temp, temp_topic))
        c.publish(press_topic, str(press), qos=1)
        print("Data publisehd {} to {}".format(press, press_topic))
    except OSError:
        print("No connection to the server. Reconnecting...")
        # while not connect_to_server():
        #     pass
    
print("Entering deepsleep")
deepsleep(config.SLEEP_TIME_S * 1000)